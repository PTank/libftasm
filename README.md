#LIBFTASM
## compilation

make

make test *create test binary to try all function*

### part one

* ft_bzero
* ft_strcat
* ft_isalpha
* ft_isdigit
* ft_isalnum
* ft_isascii
* ft_isprint
* ft_toupper
* ft_tolower

### part two

* ft_strlen
* ft_memset
* ft_memcpy
* ft_strdup
* ft_cat

### bonus

* ft_memalloc
* ft_strnew
* ft_putchar
