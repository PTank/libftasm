section	.text
	global	ft_strcat

ft_strcat:
	enter	0, 0
	mov	rbx, rsi
	mov	rcx, rdi

end_dest:
	cmp	byte[rcx], 0
	je	add_src
	inc	rcx
	jmp	end_dest

add_src:
	cmp	byte[rbx], 0
	je	exit
	mov	al, byte[rbx]
	mov	byte[rcx], al
	inc	rcx
	inc	rbx
	jmp	add_src
	
add_zero:
	cmp	byte[rcx], 0
	je	exit
	mov	byte[rcx], 0

exit:
	mov	rax, rdi
	leave
	ret

