%ifdef OSX
	%define WRITE 0x2000004
	%define READ 0x2000003
%elif LINUX
	%define WRITE 1
	%define READ 0
%endif

section	.bss
	buffer resb	10
	.size	equ $ - buffer

section	.text
	global	ft_cat

ft_cat:
	enter	0, 0
	cmp	rdi, 0
	jl	exit

core:
	push	rdi
	mov	rax, READ
	lea	rsi, [rel buffer]
	mov	rdx, buffer.size
	syscall
	jc	exitfail
	cmp	rax, 0
	jle	exitfail
	mov	rdx, rax
	mov	rax, WRITE
	mov	rdi, 1
	lea	rsi, [rel buffer]
	syscall
	cmp	rax, 0
	jl	exitfail
	pop	rdi
	jmp	core
	
exit:
	leave
	ret

exitfail:
	pop	rdi
	leave
	ret
