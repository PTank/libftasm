%ifdef OSX
	%define WRITE 0x2000004
%elif LINUX
	%define WRITE 1
%endif

section	.bss
	char	resb	1

section	.text
	global	ft_putchar

ft_putchar:
	enter	0, 0
	lea	rsi, [rel char]
	mov	byte[rsi], dil
	mov	rax, WRITE
	mov	rdx, 1
	mov	rdi, 1
	syscall
	leave
	ret
