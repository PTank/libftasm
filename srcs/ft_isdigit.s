section	.text
	global ft_isdigit

ft_isdigit:
	enter	0, 0
	cmp	rdi, 57
	jg	fail
	cmp	rdi, 48
	jl	fail
	jmp	sucess
	
fail:
	mov	rax, 0
	jmp	exit

sucess:
	mov	rax, 1

exit:
	leave
	ret
