section	.text
	global ft_bzero

ft_bzero:
	enter	0, 0	; save register
	push rdi

while:
	cmp	rsi, 0
	jle	exit
	mov	byte[rdi], 0
	inc	rdi
	dec	rsi
	jmp	while

exit:
	pop rdi
	leave		; ret to register
	ret
