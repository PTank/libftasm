extern	ft_memalloc

section	.text
	global	ft_strnew

ft_strnew:
	enter	0, 0
	push rdi
	inc	rdi
	call	ft_memalloc
	pop rdi
	leave
	ret
