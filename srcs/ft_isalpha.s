section	.text
	global ft_isalpha

ft_isalpha:
	enter	0, 0	; save register
	cmp	rdi, 122
	jg	fail
	cmp	rdi, 65
	jl	fail
	cmp	rdi, 90
	jg	test_min_low
	jmp	sucess

test_min_low:
	cmp	rdi, 97
	jl	fail
	jmp	sucess

fail:
	mov	rax, 0
	jmp	exit

sucess:
	mov	rax, 1

exit:
	leave
	ret
