extern	ft_isalpha
extern	ft_isdigit

section	.text
	global	ft_isalnum

ft_isalnum:
	enter	0, 0
	call	ft_isalpha
	cmp	rax, 1
	je	exit
	call	ft_isdigit

exit:
	leave
	ret
