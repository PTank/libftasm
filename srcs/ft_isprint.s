section	.text
	global	ft_isprint

ft_isprint:
	enter	0, 0
	cmp	rdi, 126
	jg	fail
	cmp	rdi, 32
	jl	fail
	jmp	sucess
	
fail:
	mov	rax, 0
	jmp	exit

sucess:
	mov	rax, 1

exit:
	leave
	ret
