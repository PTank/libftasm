section	.text
	global	ft_isascii

ft_isascii:
	enter	0, 0
	cmp	rdi, 127
	jg	fail
	cmp	rdi, 0
	jl	fail
	jmp	sucess
	
fail:
	mov	rax, 0
	jmp	exit

sucess:
	mov	rax, 1

exit:
	leave
	ret
