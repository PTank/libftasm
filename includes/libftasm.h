/*********************************************************************************
*     File Name           :     libftasm.h
*     Created By          :     p-tank
*     Creation Date       :     [2015-01-29 15:30]
*     Last Modified       :     [2015-02-07 11:55]
*     Description         :      
**********************************************************************************/


#ifndef _LIBFTASM_H
# define _LIBFTASM_H
# include <string.h>

	void	ft_bzero(char *str, int nb);
	size_t	ft_strlen(const char *str);
	char	*ft_strcat(char *dest, const char *src);
	int	ft_isalpha(int c);
	int	ft_isascii(int c);
	int	ft_isdigit(int c);
	int	ft_isalnum(int c);
	int	ft_isprint(int c);
	int	ft_toupper(int c);
	int	ft_tolower(int c);
	int	ft_puts(const char *s);
	void	*ft_memset(void *s, int c, size_t n);
	void	*ft_memcpy(void *dest, const void *src, size_t n);
	char	*ft_strdup(const char *s);
	void	ft_cat(int fd);
	void	*ft_memalloc(size_t size);
	char	*ft_strnew(size_t size);
	void	ft_putchar(int c);

#endif

