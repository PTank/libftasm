/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/03 14:04:14 by akazian           #+#    #+#             */
/*   Updated: 2015/02/10 12:45:10 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libftasm.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

int	main(int argc, char **argv)
{
	char	str[] = "salut";

	printf("ft_bzero and ft_strlen\n----------------------\n");
	printf("before: %s size: %d \n", str, (int)ft_strlen(str));
	ft_bzero(str, 5);
	printf("after: %s size: %d\n", str, (int)ft_strlen(str));

	char	str1[10] = "cow";
	char	str2[] = "boy";
	printf("ft_strcat\n----------------------\n");
	printf("str1: %s str2: %s\n", str1, str2);
	ft_strcat(str1, str2);
	printf("str1: %s str2: %s\n", str1, str2);

	printf("a isalpha : %s\n", (ft_isalpha('a') ? "true": "false"));
	printf("1 isalpha : %s\n", (ft_isalpha('1') ? "true": "false"));
	printf("* isalpha : %s\n", (ft_isalpha('*') ? "true": "false"));
	printf("A isascii : %s\n", (ft_isascii('A') ? "true": "false"));
	printf("(int)145 isascii : %s\n", (ft_isascii(145) ? "true": "false"));
	printf("1 idigit : %s\n", (ft_isdigit(1) ? "true": "false"));
	printf("a isigit : %s\n", (ft_isdigit('a') ? "true": "false"));
	printf("a isalnum : %s\n", (ft_isalnum('a') ? "true": "false"));
	printf("5 isalnum : %s\n", (ft_isalnum('5') ? "true": "false"));
	printf("%% isalnum : %s\n", (ft_isalnum('%') ? "true": "false"));
	printf("'4' isprint : %s\n", (ft_isprint(4) ? "true": "false"));
	printf("a ft_toupper : %c\n", ft_toupper('a'));
	printf("A ft_toupper : %c\n", ft_toupper('A'));
	printf("A ft_tolower : %c\n", ft_tolower('A'));
	printf("a ft_tolower : %c\n", ft_tolower('a'));

	printf("ft_puts\n----------------------\n");

	ft_puts("test 1");
	ft_puts(NULL);
	printf("part 2\n----------------------\n");
	char	mem[10] = {0};
	printf("ft_memset : %s\n", (char *)ft_memset(mem, 'a', 9));
	char	dest[10] = {0};
	char	src[] = "message";
	ft_memcpy(dest, src, ft_strlen(src));
	printf("ft_memcpy : %s\n", (char *)dest);

	char	*dump;

	dump = ft_strdup("copy");
	printf("ft_strdup : %s\n", dump);
	printf("ft_cat\n----------------------\n");
	int	fd2 = open(argv[0], O_RDONLY);
	ft_cat(fd2);
	ft_cat(-42);
	close(fd2);
	printf("\nbonus\n----------------------\n");
	char	*strnewtest = ft_memalloc(5);
	ft_memcpy(strnewtest, "salut", 5);
	printf("ft_strnew && ft_memalloc : %s \n", strnewtest);
	printf("ft_putchar\n----------------------\n");
	ft_putchar('a');
	ft_putchar('\n');
	return 0;
}
